#pragma once

#include "Direction.h"

class Bullet : public cocos2d::Sprite {
public:
    static Bullet *create(cocos2d::Vec2 direction);

    void update(float delta) override;

    void setDirection(cocos2d::Vec2 direction_);

private:
    const float speed = 1000.f;
    cocos2d::Vec2 direction;
};
