#pragma once

#include <iostream>
#include <stdexcept>

//
// Используется для проверки условий, без истинности
// которых, программа не может функционировать.
//
// Если условие ложно, то выполняется аварийное завершение программы.
//
#define TSU_ASSERT(cond)                      \
    if (!(cond)) {                            \
        tsu::fail(__FILE__, __LINE__, #cond); \
    }

//
// Используется для аварийного завершения программы.
//
#define TSU_FAIL(msg) tsu::fail(__FILE__, __LINE__, msg)

namespace tsu {

inline std::string makeFailMessage(const char *file, int line, const char *msg)
{
    // "file(line): invariant violation 'msg'"
    std::string res = file;
    res += '(' + std::to_string(line) + "): ";
    res += "invariant violation '" + std::string{msg} + "'";
    return res;
}

[[noreturn]] inline void fail(const char *file, int line, const char *msg)
{
    std::cerr << makeFailMessage(file, line, msg) << std::endl;
    abort();
}

}
