#include "ContactHelper.h"

ContactHelper::ContactHelper(cocos2d::PhysicsContact &contact, const std::string &mainTag)
{
    if (isTagEqualTo(contact.getShapeA(), mainTag)) {
        _main = contact.getShapeA();
        _other = contact.getShapeB();
    } else if (isTagEqualTo(contact.getShapeB(), mainTag)) {
        _main = contact.getShapeB();
        _other = contact.getShapeA();
    }
}

bool ContactHelper::wasContacted()
{
    return _main && _other;
}

cocos2d::PhysicsShape *ContactHelper::getMain()
{
    return _main;
}

cocos2d::PhysicsShape *ContactHelper::getOther()
{
   return _other;
}
