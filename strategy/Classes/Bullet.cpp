#include "Bullet.h"
#include "PhysicTags.h"
#include "Sprites.h"
#include "Screen.h"

Bullet *Bullet::create(cocos2d::Vec2 direction)
{
    // создаём объект
    auto self = new Bullet();
    self->autorelease();
    self->scheduleUpdate();
    self->direction = direction;

    // инициализируем спрайт
    self->initWithFile(sprite::bullet);

    // инициализируем физику
    auto physicBody = cocos2d::PhysicsBody::createBox(self->getContentSize());
    physicBody->setDynamic(false);
    physicBody->setContactTestBitmask(0xFFFFFFFF);
    physicBody->setName(physic_tags::bullet);
    self->setPhysicsBody(physicBody);

    return self;
}

void Bullet::update(float delta)
{
    setPosition(getPosition() + delta * speed * direction);

    // удаляем пулю, если он вылетела за пределы экрана
    if (screen::isOutOfScreen(getPosition())) {
        runAction(cocos2d::RemoveSelf::create());
    }
}

void Bullet::setDirection(cocos2d::Vec2 direction_)
{
    direction = direction_;
}
