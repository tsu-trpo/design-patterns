#pragma once

#include "Shooting.fwd.h"
#include "Bullet.h"
#include "Creature.h"
#include "Scene.h"
#include <memory>

class Shooting {
public:
    void setCreature(Creature &c)
    {
        creature = &c;
    }

    virtual float getDelay() = 0;
    virtual void fire() = 0;

protected:
    Bullet *makeDefaultBullet()
    {
        Bullet *bullet = Bullet::create(toVec2(creature->getDirection()));
        cocos2d::Vec2 offset{creature->getContentSize().width * 1.1f, 0.f};
        offset *= toVec2(creature->getDirection()).x;
        bullet->setPosition(creature->getPosition() + offset);
        return bullet;
    }

    Creature *creature = nullptr;
};

class SimpleShooting : public Shooting {
    float getDelay() override
    {
        return 2.f;
    }

    void fire() override
    {
        scene::addObject(makeDefaultBullet());
    }
};

class FastShooting : public Shooting {
    float getDelay() override
    {
        return 0.33f;
    }

    void fire() override
    {
        scene::addObject(makeDefaultBullet());
    }
};
