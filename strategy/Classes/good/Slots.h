#pragma once

#include <memory>
#include <string>
#include "Moving.h"
#include "Shooting.h"

struct ISlot {
    virtual std::string getSpritePath() = 0;
    virtual MovingPtr makeMoving(Creature &c) = 0;
    virtual ShootingPtr makeShooting(Creature &c) = 0;
};
using ISlotPtr = std::shared_ptr<ISlot>;

template <typename MovingClass, typename ShootingClass>
struct Slot : public ISlot {
    std::string spritePath;

    std::string getSpritePath() override
    {
        return spritePath;
    }

    MovingPtr makeMoving(Creature &c) override
    {
        auto moving = std::make_shared<MovingClass>();
        moving->setCreature(c);
        return moving;
    }

    ShootingPtr makeShooting(Creature &c) override
    {
        auto shooting = std::make_shared<ShootingClass>();
        shooting->setCreature(c);
        return shooting;
    }
};

template <typename M, typename S>
static ISlotPtr makeSlot(const std::string &spritePath)
{
    auto slot = std::make_shared<Slot<M, S>>();
    slot->spritePath = spritePath;
    return slot;
}
