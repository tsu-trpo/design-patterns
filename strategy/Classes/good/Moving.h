#pragma once

#include "Moving.fwd.h"
#include <memory>
#include "Creature.h"
#include "Screen.h"

static float getRandomDir()
{
    return (std::rand() % 2) ? 1.f : -1.f;
}

class Moving {
public:
    void setCreature(Creature &c)
    {
        creature = &c;
    }

    void update(float delta)
    {
        cocos2d::Vec2 oldPos = creature->getPosition();

        doUpdate(delta);

        // обновляем актуальное направление у персонажа
        cocos2d::Vec2 newPos = creature->getPosition();
        creature->setDirection((oldPos.x - newPos.x < 0.f) ? Direction::RIGHT : Direction::LEFT);

        // еслы выходим за границы экрана,
        // то появляется с противоположной стороны
        cocos2d::Vec2 pos = creature->getPosition();
        if (screen::isOutOfScreen(pos)) {
            screen::holdOnScreen(*creature);
        }
    }

protected:
    virtual void doUpdate(float delta) = 0;

    Creature *creature = nullptr;
};

class SimpleMoving : public Moving {
protected:
    void doUpdate(float delta) override
    {
        creature->setPosition(creature->getPosition() + delta * speed * direction);
    }

private:
    const float speed = 290.f;
    cocos2d::Vec2 direction{getRandomDir(), 0.f};
};

class FlyingMoving : public Moving {
protected:
    void doUpdate(float delta) override
    {
        // меняем позицию
        auto pos = creature->getPosition();
        float diffY = direction.y * delta * speed;
        pos += cocos2d::Vec2{direction.x * delta * speed,
                             diffY};
        creature->setPosition(pos);

        // проверяем не пора ли нам ...
        currentUp += diffY;
        if (currentUp > maxUp) {
            // ... спуститься вниз
            direction.y = -1.f;
        } else if (currentUp < minUp) {
            // ... или подняться
            direction.y = 1.f;
        }
    }

private:
    const float speed = 200.f;

    cocos2d::Vec2 direction{getRandomDir(), 1.f};

    float currentUp = 0.f;
    const float maxUp = 40.f;
    const float minUp = 0.f;
};
