#pragma once

#include <cocos2d.h>
#include "Assert.h"

enum class Direction {
    LEFT,
    RIGHT
};

inline cocos2d::Vec2 toVec2(Direction dir)
{
    switch (dir) {
    case Direction::RIGHT:
        return {1.f, 0.f};
    case Direction::LEFT:
        return {-1.f, 0.f};
    default:
        TSU_FAIL("Unexpected case");
    }
}


