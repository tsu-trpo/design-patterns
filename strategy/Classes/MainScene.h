#pragma once

#include <cocos2d.h>

class MainScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

private:
    CREATE_FUNC(MainScene);
    virtual bool init();
};
