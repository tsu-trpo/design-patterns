#pragma once

#include <cocos2d.h>

class Respawner : public cocos2d::Node {
public:
    explicit Respawner(cocos2d::Node &spawnPoint);
    static Respawner *create(cocos2d::Node &spawnPoint);

    void update(float delta) override;

private:
    cocos2d::Node &spawnPoint;
    const float spawnDelta = 2.5f;
    float currentTime = 0;
};
