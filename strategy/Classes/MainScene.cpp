#include "MainScene.h"
#include <SimpleAudioEngine.h>
#include "PhysicController.h"
#include "Respawner.h"
#include "Screen.h"
#include "Sprites.h"

USING_NS_CC;

namespace {

void fitScreen(Sprite *background)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    background->setScaleX(visibleSize.width / background->getContentSize().width);
    background->setScaleY(visibleSize.height / background->getContentSize().height);
}

}

Scene* MainScene::createScene()
{
    auto innerScene = MainScene::create();

    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask(0xffff);
    scene->addChild(innerScene);

    return scene;
}

bool MainScene::init()
{
    if (!Scene::init()) {
        return false;
    }
    
    auto background = Sprite::create(sprite::background);
    background->setPosition(screen::getCenter());
    fitScreen(background);
    addChild(background, 0);

    auto respawner = Respawner::create(*this);
    addChild(respawner, 0);

    auto physicController = PhysicController::create();
    addChild(physicController, 0);

    return true;
}
