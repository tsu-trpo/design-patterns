#include "Respawner.h"
#include "Creature.h"
#include "Screen.h"

Respawner::Respawner(cocos2d::Node &spawnPoint) :
    spawnPoint{spawnPoint}
{
    scheduleUpdate();
}

Respawner *Respawner::create(cocos2d::Node &spawnPoint)
{
    auto obj = new Respawner(spawnPoint);
    obj->autorelease();
    return obj;
}

void Respawner::update(float delta)
{
    currentTime += delta;
    if (currentTime < spawnDelta) {
        return;
    }
    currentTime -= spawnDelta;

    Creature *creature = makeRandomCreature();
    Vec2 offset{0.f, creature->getContentSize().height / 2.f};
    creature->setPosition(screen::getBottom() + offset);
    spawnPoint.addChild(creature);
}

