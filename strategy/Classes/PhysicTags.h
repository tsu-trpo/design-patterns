#pragma once

#include <string>

namespace physic_tags {

const std::string creature = "creature";
const std::string bullet = "bullet";

}
