#pragma once

#include <string>

namespace sprite {

const std::string background = "background.jpg";

const std::string ghost = "ghost.png";
const std::string man= "man.png";

const std::string bullet = "bullet.png";

}
