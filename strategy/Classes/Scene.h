#pragma once

#include <cocos2d.h>

namespace scene {

inline void addObject(cocos2d::Node *node)
{
    auto *scene = cocos2d::Director::getInstance()->getRunningScene();
    scene->addChild(node);
}

}
