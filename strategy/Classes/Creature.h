#pragma once

#include <string>
#include <cocos2d.h>
#include "Direction.h"
#include "good/Moving.fwd.h"
#include "good/Shooting.fwd.h"

class Creature : public cocos2d::Sprite {
public:
    static Creature *create(const std::string &spritePath);

    void setMoving(MovingPtr newMoving);
    void setShooting(ShootingPtr newShooting);

    void update(float delta) override;

    Direction getDirection();
    void setDirection(Direction newDirection);

private:
    MovingPtr moving;
    ShootingPtr shooting;

    float currentTime = 0.f;
    // все спрайты по умолчанию направлены направо
    Direction direction = Direction::RIGHT;
};

Creature *makeRandomCreature();
