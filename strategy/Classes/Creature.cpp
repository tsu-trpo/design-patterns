#include "Creature.h"
#include <vector>
#include "Assert.h"
#include "PhysicTags.h"
#include "Screen.h"
#include "Sprites.h"

#include "good/Moving.h"
#include "good/Shooting.h"
#include "good/Slots.h"

Creature *Creature::create(const std::string &spritePath)
{
    // создаём объект
    auto self = new Creature();
    self->autorelease();
    self->scheduleUpdate();

    // инициализируем спрайт
    self->initWithFile(spritePath);

    // инициализируем физику
    auto physicBody = cocos2d::PhysicsBody::createBox(self->getContentSize());
    physicBody->setDynamic(false);
    physicBody->setContactTestBitmask(0xFFFFFFFF);
    physicBody->setName(physic_tags::creature);
    self->setPhysicsBody(physicBody);

    return self;
}

void Creature::setMoving(MovingPtr newMoving)
{
    moving = newMoving;
}

void Creature::setShooting(ShootingPtr newShooting)
{
    shooting = newShooting;
}

void Creature::update(float delta)
{
    TSU_ASSERT(moving);
    TSU_ASSERT(shooting);

    // двигаемся
    moving->update(delta);

    // актуализируем спрайт в соответствии с направлением
    setScaleX(toVec2(direction).x);

    // стреляем, если можем
    currentTime += delta;
    float delay = shooting->getDelay();
    if (currentTime < delay) {
        return;
    }
    currentTime -= delay;
    shooting->fire();
}


Direction Creature::getDirection()
{
    return direction;
}

void Creature::setDirection(Direction newDirection)
{
    direction = newDirection;
}

Creature *makeRandomCreature()
{
    // список всех возможных существ
    static std::vector<ISlotPtr> slots{
        // призрак
        makeSlot<FlyingMoving, SimpleShooting>(sprite::ghost),
        // человек с автоматом
        makeSlot<SimpleMoving, FastShooting>(sprite::man),
    };

    // выбираем существо
    static size_t index = 0;
    index = (index + 1) % slots.size();
    ISlotPtr &slot = slots[index];

    // создаём существо
    auto creature = Creature::create(slot->getSpritePath());
    creature->setMoving(slot->makeMoving(*creature));
    creature->setShooting(slot->makeShooting(*creature));
    return creature;
}
