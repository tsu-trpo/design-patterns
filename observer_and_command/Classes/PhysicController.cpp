#include "PhysicController.h"
#include "Assert.h"
#include "Bullet.h"
#include "ContactHelper.h"
#include "Creature.h"
#include "PhysicTags.h"

PhysicController *PhysicController::create()
{
    // создаём объект
    auto self = new PhysicController();
    self->autorelease();

    // настраиваем handler для обработки столкновений
    self->listener = cocos2d::EventListenerPhysicsContact::create();
    self->listener->onContactBegin = CC_CALLBACK_1(PhysicController::handler, self);
    auto dispatcher = self->getEventDispatcher();
    dispatcher->addEventListenerWithSceneGraphPriority(self->listener, self);
    return self;
}

bool PhysicController::handler(cocos2d::PhysicsContact &contact)
{
    // проверяем, что пуля столкнулась ...
    ContactHelper helper{contact, physic_tags::bullet};
    if (!helper.wasContacted()) {
        return false;
    }

    // ... с существом
    cocos2d::PhysicsShape *other = helper.getOther();
    if (isTagEqualTo(other, physic_tags::creature)) {
        // наносим урон существу
        auto creature = dynamic_cast<Creature *>(other->getBody()->getNode());
        TSU_ASSERT(creature);
        creature->takeDamage();

        // уничтожаем пулю
        cocos2d::PhysicsShape *main = helper.getMain();
        auto bullet = main->getBody()->getNode();
        bullet->runAction(cocos2d::RemoveSelf::create());

        return true;
    }

    return false;
}
