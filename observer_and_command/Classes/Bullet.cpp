#include "Bullet.h"
#include "PhysicTags.h"
#include "Sprites.h"
#include "Screen.h"

namespace {

float getRandomSpeed()
{
    const float min = 700.f;
    const float max = 2000.f;
    return min + cocos2d::rand_0_1() * (max - min);
}

}


Bullet *Bullet::create(cocos2d::Vec2 direction)
{
    // создаём объект
    auto self = new Bullet();
    self->autorelease();
    self->scheduleUpdate();
    self->direction = direction;

    // инициализируем спрайт
    self->initWithFile(sprite::bullet);

    // инициализируем физику
    auto physicBody = cocos2d::PhysicsBody::createBox(self->getContentSize());
    physicBody->setDynamic(false);
    physicBody->setContactTestBitmask(0xFFFFFFFF);
    physicBody->setName(physic_tags::bullet);
    self->setPhysicsBody(physicBody);

    // инициализируем поля
    self->speed = getRandomSpeed();

    return self;
}

Bullet *Bullet::create(Direction direction)
{
    return Bullet::create(toVec2(direction));
}

void Bullet::update(float delta)
{
    setPosition(getPosition() + delta * speed * direction);

    // удаляем пулю, если он вылетела за пределы экрана
    if (screen::isOutOfScreen(getPosition())) {
        runAction(cocos2d::RemoveSelf::create());
    }
}
