#pragma once

#include <string>
#include <cocos2d.h>
#include "HealthObserver.h"

class Creature : public cocos2d::Sprite, public HealthSubject {
public:
    static Creature *create(const std::string &spritePath);

    void update(float delta) override;

    void jump();
    void land();

    void delayedInit();
    void takeDamage();

private:
    bool isAlreadyInited = false;

    enum class State {
        IDLE,
        JUMPING,
        LANDING
    };
    State state = State::IDLE;

    int health = 10;

    const float jumpForce = 20.f;
    const float landForce = -20.f;
    const float gravity = -60.f;
    float velocityY = 0.f;
    float currentY = 0.f;
};
