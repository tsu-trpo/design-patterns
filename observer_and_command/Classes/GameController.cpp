#include "GameController.h"
#include "MainScene.h"

GameController *GameController::create()
{
    auto self = new GameController();
    self->autorelease();

    return self;
}

void GameController::restart()
{
    auto newScene = MainScene::createScene();
    cocos2d::Director::getInstance()->replaceScene(newScene);
}
