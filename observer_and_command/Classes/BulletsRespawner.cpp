#include "BulletsRespawner.h"
#include "Bullet.h"
#include "Screen.h"

BulletsRespawner::BulletsRespawner(cocos2d::Node &spawnPoint) :
    spawnPoint{spawnPoint}
{
    scheduleUpdate();
}

BulletsRespawner *BulletsRespawner::create(cocos2d::Node &spawnPoint)
{
    auto obj = new BulletsRespawner(spawnPoint);
    obj->autorelease();
    return obj;
}

void BulletsRespawner::update(float delta)
{
    currentTime += delta;
    if (currentTime < spawnDelta) {
        return;
    }
    currentTime -= spawnDelta;

    Direction direction;
    Vec2 position;

    if (rand_0_1() < 0.5f) {
        // выстрел с правой стороны
        direction = Direction::LEFT;
        position = background::getRightRoadPoint();
    } else {
        // выстрел с левой стороны
        direction = Direction::RIGHT;
        position = background::getLeftRoadPoint();
    }

    Bullet *bullet = Bullet::create(direction);
    Vec2 shift{0.f, 60.f};
    bullet->setPosition(position + shift);
    spawnPoint.addChild(bullet);
}

