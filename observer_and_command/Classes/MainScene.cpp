#include "MainScene.h"
#include <SimpleAudioEngine.h>
#include "BulletsRespawner.h"
#include "Creature.h"
#include "GameController.h"
#include "HealthBar.h"
#include "HeroController.h"
#include "PhysicController.h"
#include "Screen.h"
#include "Sprites.h"

USING_NS_CC;

namespace {

void fitScreen(Sprite *background)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    background->setScaleX(visibleSize.width / background->getContentSize().width);
    background->setScaleY(visibleSize.height / background->getContentSize().height);
}

}

Scene* MainScene::createScene()
{
    auto innerScene = MainScene::create();

    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask(0xffff);
    scene->addChild(innerScene);

    return scene;
}

bool MainScene::init()
{
    if (!Scene::init()) {
        return false;
    }
    
    auto background = Sprite::create(sprite::background);
    background->setPosition(screen::getCenter());
    fitScreen(background);
    addChild(background, 0);

    auto respawner = BulletsRespawner::create(*this);
    addChild(respawner, 0);

    auto hero = Creature::create(sprite::hero);
    hero->setScale(0.5f);
    auto shift = Vec2{0.f, hero->getScale() * hero->getContentSize().height / 2.f};
    hero->setPosition(background::getCenterOfRoad() + shift);
    addChild(hero, 1);

    auto heroController = HeroController::create(hero);
    addChild(heroController);

    auto healthBar = HealthBar::create(*hero);
    addChild(healthBar, 1);

    auto gameController = GameController::create();
    addChild(gameController);

    auto physicController = PhysicController::create();
    addChild(physicController, 0);

    return true;
}
