#include "AudioPlayer.h"
#include <string>
#include <cocos2d.h>
#include <SimpleAudioEngine.h>

namespace {

CocosDenshion::SimpleAudioEngine &getPlayer()
{
    return *CocosDenshion::SimpleAudioEngine::getInstance();
}

void playBackgroundMusic(const char *music)
{
    getPlayer().playBackgroundMusic(music, true);
}

}

void AudioPlayer::playSimpleMusic()
{
    playBackgroundMusic("music/simple.mp3");
}

void AudioPlayer::playPanicMusic()
{
    playBackgroundMusic("music/panic.mp3");
}
