#pragma once

#include <string>

namespace font {

const std::string arial = "fonts/arial.ttf";
const std::string anagram = "fonts/Anagram.ttf";

}