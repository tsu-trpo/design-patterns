#pragma once

#include <string>

namespace sprite {

const std::string background = "background.jpg";

const std::string hero = "hero.png";
const std::string bullet = "bullet.png";

const std::string heart = "heart.png";

}
