#include "Creature.h"
#include <vector>
#include "PhysicTags.h"

Creature *Creature::create(const std::string &spritePath)
{
    // создаём объект
    auto self = new Creature();
    self->autorelease();
    self->scheduleUpdate();

    // инициализируем спрайт
    self->initWithFile(spritePath);

    // инициализируем физику
    auto physicBody = cocos2d::PhysicsBody::createBox(self->getContentSize());
    physicBody->setDynamic(false);
    physicBody->setContactTestBitmask(0xFFFFFFFF);
    physicBody->setName(physic_tags::creature);
    self->setPhysicsBody(physicBody);

    return self;
}

void Creature::update(float delta)
{
    delayedInit();

    if (state == State::JUMPING || state == State::LANDING) {
        velocityY += gravity * delta;
        setPositionY(getPositionY() + velocityY);
        currentY += velocityY;

        // выравниваемся, если опустились
        // ниже первоначального уровня
        if (currentY <= 0.f) {
            setPositionY(getPositionY() - currentY);
            currentY = 0.f;
            state = State::IDLE;
        }
    }
}

void Creature::jump()
{
    if (state == State::IDLE) {
        state = State::JUMPING;

        velocityY = jumpForce;
    }
}

void Creature::land()
{
    if (state == State::JUMPING) {
        state = State::LANDING;

        velocityY = landForce;
    }
}

void Creature::delayedInit()
{
    if (isAlreadyInited) {
        return;
    }
    isAlreadyInited = true;

    notify(health);
}

void Creature::takeDamage()
{
    --health;
    notify(health);
}
