#pragma once

#include <cocos2d.h>

class BulletsRespawner : public cocos2d::Node {
public:
    explicit BulletsRespawner(cocos2d::Node &spawnPoint);
    static BulletsRespawner *create(cocos2d::Node &spawnPoint);

    void update(float delta) override;

private:
    cocos2d::Node &spawnPoint;
    const float spawnDelta = 1.3f;
    float currentTime = 0;
};
