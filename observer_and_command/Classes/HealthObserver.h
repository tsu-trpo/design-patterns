#pragma once

#include <memory>
#include <vector>

class HealthObserver {
public:
    virtual ~HealthObserver() = default;
    virtual void onNotify(int health) = 0;
};

class HealthSubject {
public:
    void addObserver(HealthObserver &observer);
    void removeObserver(HealthObserver &observer);
    void notify(int health);

private:
    std::vector<HealthObserver *> observers;
};
