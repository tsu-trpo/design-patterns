#include "HealthBar.h"
#include "Screen.h"
#include "Sprites.h"
#include "Fonts.h"

HealthBar *HealthBar::create(HealthSubject &subject)
{
    // создаём объект
    auto self = new HealthBar();
    self->autorelease();
    // подписываемся на оповещения о кол-ве жизней
    subject.addObserver(*self);

    // инициализируем спрайт
    auto sprite = Sprite::create(sprite::heart);
    sprite->setScale(0.1f);
    self->addChild(sprite);

    // инициализируем текст
    std::string text = "-";
    int fontSize = 90;
    auto label = self->label = Label::createWithTTF(text, font::anagram, fontSize);
    label->setAnchorPoint(anchor::right);
    self->addChild(label);

    // позиционируем объекты на экране
    auto shift = sprite->getContentSize() * sprite->getScale();
    self->setPosition(screen::getUpRightCorner() - shift);

    label->setPositionX(label->getPositionX() - shift.width / 2.f);

    return self;
}

void HealthBar::onNotify(int health)
{
    label->setString(std::to_string(health));
}
