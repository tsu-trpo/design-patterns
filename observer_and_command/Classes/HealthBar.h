#pragma once

#include <cocos2d.h>
#include "HealthObserver.h"

class HealthBar : public cocos2d::Node, public HealthObserver {
public:
    static HealthBar *create(HealthSubject &subject);

    void onNotify(int health) override;

private:
    cocos2d::Label *label = nullptr;
};
