#pragma once

#include <cocos2d.h>

USING_NS_CC;

namespace screen {

inline bool isOutOfScreen(const Vec2 &point)
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();

    return point.x < origin.x ||
           point.x > visibleSize.width ||
           point.y < origin.y ||
           point.y > visibleSize.height;
}

inline void holdOnScreen(Node &node)
{
    auto pos = node.getPosition();
    cocos2d::Vec2 visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
    node.setPositionX(((int)pos.x + (int)visibleSize.x) % (int)visibleSize.x);
}

inline Vec2 getCenter()
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    return Vec2{origin.x + visibleSize.width / 2.0f,
                origin.y + visibleSize.height / 2.0f};
}

inline Vec2 getBottomLeftCorner()
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    return Vec2{origin.x, origin.y};
}

inline Vec2 getBottom()
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    return Vec2{origin.x + visibleSize.width / 2.0f,
                origin.y};
}

inline Vec2 getBottomRightCorner()
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    return Vec2{origin.x + visibleSize.width,
                origin.y};
}

inline Vec2 getUpRightCorner()
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    return origin + visibleSize;
}

}

namespace anchor {

const Vec2 bottom = {0.5f, 0.f};
const Vec2 right = {1.f, 0.5f};

}

namespace background {

const float shiftBetweenScreenAndRoad = 190.f;

inline Vec2 getCenterOfRoad()
{
    return screen::getBottom() + Vec2{0.f, shiftBetweenScreenAndRoad};
}

inline Vec2 getLeftRoadPoint()
{
    return screen::getBottomLeftCorner() + Vec2{0.f, shiftBetweenScreenAndRoad};
}

inline Vec2 getRightRoadPoint()
{
    return screen::getBottomRightCorner() + Vec2{0.f, shiftBetweenScreenAndRoad};
}

}
