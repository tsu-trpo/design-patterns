#pragma once

#include "Creature.h"

class Command {
public:
    virtual ~Command() = default;

    virtual void execute() = 0;
};

class JumpCommand : public Command {
public:
    explicit JumpCommand(Creature &hero) :
        hero{hero}
    {
    }

    void execute() override
    {
        hero.jump();
    }

    Creature &hero;
};
