#include "HealthObserver.h"
#include <algorithm>

void HealthSubject::addObserver(HealthObserver &observer)
{
    observers.push_back(&observer);
}

void HealthSubject::removeObserver(HealthObserver &observer)
{
    auto found = std::find(observers.begin(), observers.end(), &observer);
    if (found != observers.end()) {
        observers.erase(found);
    }
}

void HealthSubject::notify(int health)
{
    for (auto &o : observers) {
        o->onNotify(health);
    }
}
