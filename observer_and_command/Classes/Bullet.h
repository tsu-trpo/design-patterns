#pragma once

#include "Direction.h"

class Bullet : public cocos2d::Sprite {
public:
    static Bullet *create(cocos2d::Vec2 direction);
    static Bullet *create(Direction direction);

    void update(float delta) override;

private:
    float speed;
    cocos2d::Vec2 direction;
};
