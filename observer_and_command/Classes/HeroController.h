#pragma once

#include <memory>
#include <cocos2d.h>
#include "Creature.h"
#include "Commands.h"

using KeyCode = cocos2d::EventKeyboard::KeyCode;

class HeroController : public cocos2d::Node {
public:
    static HeroController *create(Creature *hero);

private:
    KeyCode onKeyboardPressed(KeyCode keyCode, cocos2d::Event* event);

    std::shared_ptr<Command> upButton;
    std::shared_ptr<Command> downButton;
};
