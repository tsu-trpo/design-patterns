#include "HeroController.h"

HeroController *HeroController::create(Creature *hero)
{
    auto self = new HeroController();
    self->autorelease();

    // подписка на события о нажатии клавиш
    auto listener = cocos2d::EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(HeroController::onKeyboardPressed, self);
    self->_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, self);

    // инициализация полей
    self->upButton = std::make_shared<JumpCommand>(*hero);

    return self;
}

KeyCode HeroController::onKeyboardPressed(KeyCode keyCode, cocos2d::Event* event)
{
    switch (keyCode)
    {
    case KeyCode::KEY_W:
    case KeyCode::KEY_UP_ARROW:
        upButton->execute();
    break;

    case KeyCode::KEY_S:
    case KeyCode::KEY_DOWN_ARROW:
        // тут должен быть вызов
        // downButton->execute(), но команда
        // для обработки так и не установлена
    break;
    }

    return keyCode;
}
